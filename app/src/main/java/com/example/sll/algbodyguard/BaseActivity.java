package com.example.sll.algbodyguard;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class BaseActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        init();
    }
    /**
     * 上一步
     *
     * @param v
     */
    public void pre(View v) {

    }

    /**
     * 下一步
     *
     * @param v
     */
    public abstract void next(View v);

    /**
     * 初始化操作
     */
    public abstract void init();

    /**
     * 初始化头部
     *
     * @param title
     */
    protected void initHeader(String title) {
        TextView tv_title = (TextView) this.findViewById(R.id.tv_title);
        tv_title.setText(title);
    }
    /**
     * 初始化中间点
     *
     * @param index
     */
    protected void initPoint(int index) {
        LinearLayout ll_points = (LinearLayout) findViewById(R.id.ll_points);
        // 获得线性布局下面的孩子
        ImageView iv = (ImageView) ll_points.getChildAt(index);
//设置背景颜色
        iv.setImageResource(android.R.drawable.presence_online);
    }

}
