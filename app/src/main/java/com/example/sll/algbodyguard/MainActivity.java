package com.example.sll.algbodyguard;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.sll.algbodyguard.util.Utils;

public class MainActivity extends AppCompatActivity {

    private GridView gv_home_gridview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //初始化GridView
        gv_home_gridview = (GridView) findViewById(R.id.gv_home_gridview);
        gv_home_gridview.setAdapter(new MyAdapter(MainActivity.this));
    }

    //点击Item，实现跳转到设置密码界面
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub
        switch (arg2) {
            case 0:
        //Toast.makeText(this, "点我啦", Toast.LENGTH_SHORT).show();
                //跳转到设置密码界面
                Utils.startActivity(this, PasswrodActivity.class);
                break;
            default:
                break;
        }

    }


}
