package com.example.sll.algbodyguard.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class Utils {
    //跳转功能
    public static void startActivity(Context context,
                                     Class<? extends Activity> clazz) {
        //意图
        Intent intent = new Intent(context, clazz);
        //转跳
        context.startActivity(intent);
    }
    //

}
