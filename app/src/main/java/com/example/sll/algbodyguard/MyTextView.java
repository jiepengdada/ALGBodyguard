package com.example.sll.algbodyguard;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class MyTextView extends android.support.v7.widget.AppCompatTextView {
    public MyTextView(Context context) {
        super(context);
    }
    //定义在xml文件中用的，布局文件是代码的辅助形式，都是可以用代码写出来的，一些公司为了防止别人抄袭布局，项目可以全部用代码实现
    //AttributeSet:保存了控件在布局文件的所有属性
    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

    }
    //定义在xml文件中用的,并带有一定的样式
    public MyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }
    //上面三步和系统的TextView是一模一样的，我们这里制作跑马灯，需要获取到他的焦点，就要设置自定义的TextView获取焦点
    //让系统知道当前控件是能够获得焦点的
    //true 获取  false 不获取
    @Override
    public boolean isFocused() {
        return true;
    }
}

