package com.example.sll.algbodyguard;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAdapter extends BaseAdapter {
    //用一个数字先把条目名字装好
    private String[] items = { "手机防盗", "黑名单", "软件管理", "进程管理", "流量统计", "手机杀毒", "缓存清理", "高级工具", "设置中心" };
    //这里是用到的Selector选择器，可以设置按下和放开两种不同的状态图片显示不用
    private int[] imgs = { R.drawable.mobile_guard_selector, R.drawable.communication_guards, R.drawable.app_manager, R.drawable.process_manager,R.drawable.traffic_statistics, R.drawable.mobile_kill_virus, R.drawable.cache_clear, R.drawable.advanced_tools, R.drawable.settings_center };
    private Context context;
    public MyAdapter(Context context){
        this.context=context;
    }
    // ListView条目的个数
    @Override
    public int getCount() {
        return items.length;
    }
    //获取条目数据
    @Override
    public Object getItem(int position) {
        return null;
    }
    //获取条目的id
    @Override
    public long getItemId(int position) {
        return 0;
    }
    //设置条目的样式 ，positon代表条目的位置，从0开始的
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
// 填充布局
        View v = View.inflate(context, R.layout.gv_item, null);
//找到控件
        ImageView iv_icon=(ImageView) v.findViewById(R.id.iv_icon);
        TextView tv_content=(TextView) v.findViewById(R.id.tv_content);
//设置图片
        iv_icon.setImageResource(imgs[position]);
//设置文字
        tv_content.setText(items[position]);
        return v;

    }
}
